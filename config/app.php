<?php

return [
    'key' => env('APP_KEY'),
    'url' => env('APP_URL'),
    'cipher' => 'AES-256-CBC',
];
