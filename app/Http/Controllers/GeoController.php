<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Geo;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class GeoController extends Controller
{
    public function list(Request $request)
    {
        $offset = $request->get('offset');
        if ($offset === null) {
            $offset = 0;
        }

        $count = $request->get('count');
        if ($count === null) {
            $count = 50;
        }

        $geos = DB::table('geos')
            ->skip($offset)
            ->take($count)
            ->get();

        return $this->response([
            'offset' => (int)$offset,
            'count' => (int)$count,
            'entries' => $this->prepareEntries($geos),
        ]);
    }

    private function prepareEntries($geos): array
    {
        return [];
    }
}
