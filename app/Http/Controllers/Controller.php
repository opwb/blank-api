<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    /**
     * @param string[] $messages
     * @param array $data
     *
     * @return JsonResponse
     */
    public function response(array $data, array $messages = []) : JsonResponse
    {
        return response()->json([
            'status' => 'ok',
            'messages' => $messages,
            'data' => $data,
        ]);
    }

    /**
     * @param array    $request
     * @param string[] $messages
     *
     * @return JsonResponse
     */
    public function error(array $request, array $messages) : JsonResponse
    {
        // response to client
        return response()->json([
            'status' => 'error',
            'messages' => $messages,
        ]);
    }
}
